
# read data
dat = read.csv("../data/pbdb_data_for Rachel_species.csv")

# generate fixed fossil ages
dat$fixed_ma = rowMeans(dat[,c("max_ma","min_ma")])

# timeline
name = unique(c(as.vector(dat$early_interval), as.vector(dat$late_interval)))
name = sort(name)
name = name[-1] # remove empty string from position one

# remove <stage level resolution (e.g. Early Albian = Albian)
name = gsub("Early ", "", name)
name = gsub("Middle ", "", name)
name = gsub("Late ", "", name)
name = unique(name)

# Remove entries only resolved to the level of period
if(length(which(name == "Triassic")) > 0) name = name[-which(name == "Triassic")]
if(length(which(name == "Jurassic")) > 0) name = name[-which(name == "Jurassic")]
if(length(which(name == "Cretaceous")) > 0) name = name[-which(name == "Cretaceous")]

# -> at this point should all names = stages

# watch out: created manually!
epoch = c("Middle Jurassic", #Aalenian
          "Late Triassic", #Alaunian = Middle Norian
          "Early Cretaceous", #Albian
          "Middle Triassic", #Anisian
          "Early Cretaceous", #Aptian
          "Middle Jurassic", #Bajocian
          "Early Cretaceous", #Barremian
          "Middle Jurassic", #Bathonian
          "Early Cretaceous", #Berriasian
          "Middle Jurassic", #Callovian
          "Late Cretaceous", #Campanian
          "Late Triassic", #Carnian
          "Late Cretaceous", #Cenomanian
          "Late Cretaceous", # Coniacian
          "Early Cretaceous", #Hauterivian
          "Early Jurassic", #Hettangian
          "Late Jurassic", #Kimmeridgian
          "Late Cretaceous", #Maastrichtian
          "Late Jurassic", #Oxfordian
          "Paleocene", #Paleocene <- eliminate these entries
          "Late Cretaceous", #Santonian
          "Early Jurassic", #Sinemurian
          "Late Jurassic", #Tithonian
          "Early Jurassic", #Toarcian
          "Late Cretaceous", #Turonian
          "Early Cretaceous", #Valanginian
          "Late Cretaceous", #Edmontonian
          "Late Cretaceous", #Judithian
          "Late Triassic", #Lacian
          "Late Cretaceous", #Lancian
          "Early Cretaceous", #Neocomian
          "Late Triassic", #Norian
          "Early Jurassic", #Pliensbachian
          "Late Triassic", #Rhaetian
          "Late Cretaceous", #Senonian
          "Early Jurassic") #Tenuicostatum

geoscale = data.frame(epoch = epoch, int = name)

# get rid of birds
dat2 = dat[-which(dat$Bird == 1),]

# dates

clean = function(j){
  j = gsub("Early ", "", j)
  j = gsub("Middle ", "", j)
  j = gsub("Late ", "", j)
  return(j)
}

dat2$early_epoch = "tbc"
dat2$late_epoch = "tbc"

for(i in 1:length(dat2[,1])){
  
  early.int = dat2$early_interval[i]
  late.int = dat2$late_interval[i]
  if(late.int == "") late.int = early.int
  
  # deal with early.int
  if(grepl("Triassic", early.int) | grepl("Jurassic", early.int) | grepl("Creataceous", early.int)){
    dat2$early_epoch[i] = as.character(early.int)
  } else {
    early.int = clean(early.int)
    dat2$early_epoch[i] = as.character(geoscale$epoch[which(geoscale$int == as.character(early.int))])
  }
  
  # deal with late.int
  if(grepl("Triassic", late.int) | grepl("Jurassic", late.int) | grepl("Creataceous", late.int)){
    dat2$late_epoch[i] = as.character(late.int)
  } else {
    late.int = clean(late.int)
    dat2$late_epoch[i] = as.character(geoscale$epoch[which(geoscale$int == as.character(late.int))])
  }
  
}

# eliminate specimens with age uncertainty > 1 epoch
dat2 = dat2[-which(dat2$early_epoch != dat2$late_epoch),]

# model 1 requires FA, LAs, k
## ranges

use.means = TRUE
offset = 0 # 66

sp = c()
FA = c()
LA = c()
for(i in unique(dat2$accepted_name)){
  
  sp = c(sp, gsub(" ", "_", i))
  
  d = subset(dat2, dat2$accepted_name == i)
  
  if(use.means){
    FA = c(FA, max(d$fixed_ma) - offset)
    LA = c(LA, min(d$fixed_ma) - offset)
  } else {
    if(max(d$min_ma) < min(d$max_ma)) stop("this won't work")
    FA = c(FA, max(d$min_ma) - offset)
    LA = c(LA, min(d$max_ma) - offset)
  }
  
}

write.table(data.frame(taxon = sp, min = LA, max = FA), row.names = FALSE, quote = FALSE, file = "rev_coarse_species/ranges.txt", sep = "\t")

## intervals

name = c("Paleogene", "Late Cretaceous", "Early Cretaceous", 
         "Late Jurassic", "Middle Jurassic", "Early Jurassic",
         "Late Triassic", "Middle Triassic")

max = c(66.0, 100.5, 145.0, 163.5, 174.1, 201.3, 237, 247.2)

min = c(0, 66.0, 100.5, 145.0, 163.5, 174.1, 201.3, 237)

df = data.frame(name = name, max = max, min = min, k = 0)

### the following is for checking the data
for(i in 1:length(dat2$occurrence_no)){
  
  epoch = dat2$early_epoch[i]
  age = dat2$fixed_ma[i]
  
  max = df[which(df$name == epoch),]$max
  min = df[which(df$name == epoch),]$min
  
  if(age > max | age < min) {
    cat("problem with ", i, dat2$occurrence_no[i], 
    as.character(dat2$early_interval[i]), as.character(dat2$late_interval[i]), "\n")
  }
  
}
### -> there appears to be an issue with the Callovian, 
### specimen ages conflict with official geological timescale

### count the number of fossils
new = c()
for(i in 1:length(df$name)){
  
  d = subset(dat2, dat2$early_epoch == df$name[i])
  k = length(d$occurrence_no)
  
  df$k[i] = k
  
  new = c(new, gsub(" ", "_", as.character(df$name[i])))
  
}

df$name = new

write.table(df, row.names = FALSE, quote = FALSE, file = "rev_coarse_species/intervals.txt", sep = "\t")

# model 3 requires 1/0 data
df = data.frame(sp = character(), int1 = numeric(), int2 = numeric(), int3 = numeric(), 
                int4 = numeric(), int5 = numeric(), int6 = numeric(), int7 = numeric(), int8 = numeric())

for(i in unique(dat2$accepted_name)){
  
  sp = gsub(" ", "_", i)
  
  d = subset(dat2, dat2$accepted_name == i)
  
  # int7 = ifelse("Middle Triassic" %in% d$early_epoch, 1, 0)
  # int6 = ifelse("Late Triassic" %in% d$early_epoch, 1, 0)
  # int5 = ifelse("Early Jurassic" %in% d$early_epoch, 1, 0)
  # int4 = ifelse("Middle Jurassic" %in% d$early_epoch, 1, 0)
  # int3 = ifelse("Late Jurassic" %in% d$early_epoch, 1, 0)
  # int2 = ifelse("Early Cretaceous" %in% d$early_epoch, 1, 0)
  # int1 = ifelse("Late Cretaceous" %in% d$early_epoch, 1, 0)
  
  int1 = length(which(d$early_epoch == "Paleogene"))
  int2 = length(which(d$early_epoch == "Late Cretaceous"))
  int3 = length(which(d$early_epoch == "Early Cretaceous"))
  int4 = length(which(d$early_epoch == "Late Jurassic"))
  int5 = length(which(d$early_epoch == "Middle Jurassic"))
  int6 = length(which(d$early_epoch == "Early Jurassic"))
  int7 = length(which(d$early_epoch == "Late Triassic"))
  int8 = length(which(d$early_epoch == "Middle Triassic"))
  
  df = rbind(df, data.frame(sp = sp, int1 = int1, int2 = int2, int3 = int3, 
                  int4 = int4, int5 = int5, int6 = int6, int7 = int7, int8 = int8))
  
}

write.table(df, row.names = FALSE, quote = FALSE, file = "rev_coarse_species/presence.absence.txt", sep = "\t")

# length(which(dat2$early_epoch == "Middle Triassic"))
# length(which(dat2$early_epoch == "Late Triassic"))
# length(which(dat2$early_epoch == "Early Jurassic"))
# length(which(dat2$early_epoch == "Middle Jurassic"))
# length(which(dat2$early_epoch == "Late Jurassic"))
# length(which(dat2$early_epoch == "Early Cretaceous"))
# length(which(dat2$early_epoch == "Late Cretaceous"))

