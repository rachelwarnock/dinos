## next level
rev.plot.div.rates.new = function(output,fig.types=c("speciation rate", "extinction rate", "net-diversification rate","relative-extinction rate"),
                                  xlab="million years ago", col=NULL, col.alpha=50, offset = 0,
                                  xaxt="n",yaxt="s", pch=19, plot.tree=FALSE, use.geoscale=TRUE, use.smoothing=!TRUE,
                                  predictor.ages=c(), predictor.var=c(), labels = NULL,...){
  
  # Check that fig type is valid
  validFigTypes <- c("speciation rate","speciation shift times","speciation Bayes factors",
                     "extinction rate","extinction shift times","extinction Bayes factors",
                     "fossilization rate","fossilization shift times","fossilization Bayes factors",
                     "net-diversification rate","relative-extinction rate",
                     "mass extinction times","mass extinction Bayes factors")
  invalidFigTypes <- fig.types[!fig.types %in% validFigTypes]
  
  if ( length( invalidFigTypes ) > 0 ) {
    stop("\nThe following figure types are invalid: ",paste(invalidFigTypes,collapse=", "),".",
         "\nValid options are: ",paste(validFigTypes,collapse=", "),".")
  }         
  
  # Check tree arguments
  if(plot.tree & is.null(output$tree))
    stop("You must provide a tree if plot.tree = TRUE")
  
  # Make color vector
  if ( is.null(col) ) {
    col <- c("speciation rate"="#984EA3",
             "speciation shift times"="#984EA3",
             "speciation Bayes factors"="#984EA3",
             "extinction rate"="#E41A1C",
             "extinction shift times"="#E41A1C",
             "extinction Bayes factors"="#E41A1C",
             "fossilization rate"="#ffa31a",
             "fossilization shift times"="#ffa31a",
             "fossilization Bayes factors"="#ffa31a",
             "net-diversification rate"="#377EB8",
             "relative-extinction rate"="#FF7F00",
             "mass extinction times"="#4DAF4A",
             "mass extinction Bayes factors"="#4DAF4A")
  } else {
    names(col) <- fig.types         
  }
  
  # Compute the axes
  if(!is.null(output$tree)){
    tree_age <- max(node.depth.edgelength(output$tree))
  } else {
    tree_age <- max(output$intervals)
  }
  numIntervals <- length(output$intervals)-1
  plotAt <- 0:numIntervals
  intervalSize <- tree_age/numIntervals
  #labels <- pretty(c(0,tree_age)) + offset
  #labelsAt <- numIntervals - ( pretty(c(0,tree_age)) / intervalSize)
  #if(is.null(labels)){
  #labels <- pretty(c(min(output$intervals),max(output$intervals))) + offset
  
  # ^ to find the above search for the last useage of "rev.plot.div.rates.new = function" 
  # timestamp = 2019-02-19 11:19 - it should appear HIGHEST in the list
  # v next bit search for "#labels <- pretty(c(min(output$intervals),max(output$intervals))) + offset"
  
  #  labelsAt <- numIntervals - ( pretty(c(min(output$intervals),max(output$intervals))) / intervalSize )
  #} else{
  labelsAt <- numIntervals - labels + offset
  #}
  ages <- seq(0,tree_age,length.out=numIntervals+1)
  for( type in fig.types ) {
    if ( grepl("times",type) ) {
      thisOutput <- output[[type]]
      meanThisOutput <- colMeans(thisOutput)
      criticalPP <- output[[grep(strsplit(type," ")[[1]][1],grep("CriticalPosteriorProbabilities",names(output),value=TRUE),value=TRUE)]]
      if(plot.tree){
        plot(output$tree,show.tip.label=FALSE,edge.col=rgb(0,0,0,0.10),x.lim=c(0,treeAge))
        par(new=TRUE)
      }
      barplot(meanThisOutput,space=0,xaxt=xaxt,col=col[type],border=col[type],main=type,ylab="posterior probability",xlab=xlab,ylim=c(0,1),...)
      abline(h=criticalPP,lty=2,...)
      axis(4,at=criticalPP,labels=2*log(output$criticalBayesFactors),las=1,tick=FALSE,line=-0.5)
      axis(1,at=labelsAt,labels=labels)
      box()
    } else if ( grepl("Bayes factors",type) ) {
      thisOutput <- output[[type]]
      ylim <- range(c(thisOutput,-10,10),finite=TRUE)
      if(plot.tree){
        plot(output$tree,show.tip.label=FALSE,edge.col=rgb(0,0,0,0.10),x.lim=c(0,tree_age))
        par(new=TRUE)
      }
      plot(x=plotAt[-1]-diff(plotAt[1:2])/2,y=thisOutput,type="p",xaxt=xaxt,col=col[type],ylab="Bayes factors",main=type,xlab=xlab,ylim=ylim,xlim=range(plotAt),pch=pch,...)
      abline(h=2 * log(output$criticalBayesFactors),lty=2,...)
      axis(4,at=2 * log(output$criticalBayesFactors),las=1,tick=FALSE,line=-0.5)
      axis(1,at=labelsAt,labels=labels)
    } else if ( grepl("shift prob",type) ) {
      thisOutput <- output[[type]]
      ylim <- range(c(thisOutput,0,1),finite=TRUE)
      # skipping if plot tree bit
      plot(x=plotAt[-1]-diff(plotAt[1:2])/2,y=thisOutput,type="p",xaxt=xaxt,col=col[type],ylab="Posterior Probability",main=type,xlab=xlab,ylim=ylim,xlim=range(plotAt),pch=pch,...)
      # skipping commented out bit
    } else {
      thisOutput <- output[[type]]
      meanThisOutput <- colMeans(thisOutput)
      quantilesThisOutput <- apply(thisOutput,2,quantile,prob=c(0.025,0.975))
      # find the limits of the y-axis
      # we always want the speciation and extinction rates to be on the same scale
      if ( type %in% c("speciation rate","extinction rate")) {
        quantilesSpeciation <- apply(output[["speciation rate"]],2,quantile,prob=c(0.025,0.975))
        quantilesExtinction <- apply(output[["extinction rate"]],2,quantile,prob=c(0.025,0.975))
        
        # v next bit search for "quantilesExtinction <- apply(output[["extinction rate"]],2,quantile,prob=c(0.025,0.975))"
        ylim <- c(min(0,quantilesSpeciation,quantilesExtinction),max(quantilesSpeciation,quantilesExtinction))
      } else {
        ylim <- c(min(0,quantilesThisOutput),max(quantilesThisOutput))
      }
      # skipping if plot tree
      #### plot
      # skipping if use.geoscale == TRUE, use.smoothing == TRUE
      
      ylab = type
      #if(type == "fossilization rate" | type == "net-diversification rate"){
      if(type == "net-diversification rate"){
        plot(x=plotAt,y=c(meanThisOutput[1],meanThisOutput),type="l",ylim=ylim,xaxt=xaxt,col=col[type],ylab=type,main=NULL,xlab=xlab,cex.lab=2,cex.axis=2,bty="n",...)
        polygon(x=c(0:ncol(quantilesThisOutput),ncol(quantilesThisOutput):0),y=c(c(quantilesThisOutput[1,1],quantilesThisOutput[1,]),rev(c(quantilesThisOutput[2,1],quantilesThisOutput[2,]))),border=NA,col=paste(col[type],col.alpha,sep=""))
        axis(1,at=labelsAt,labels=labels,cex.lab=1.5, cex.axis=1.5)
      } else {
        plot(x=plotAt,y=c(meanThisOutput[1],meanThisOutput),type="l",ylim=ylim,xaxt=xaxt,col=col[type],ylab=type,main=NULL,xlab="",cex.lab=2, cex.axis=2,bty="n",...)
        polygon(x=c(0:ncol(quantilesThisOutput),ncol(quantilesThisOutput):0),y=c(c(quantilesThisOutput[1,1],quantilesThisOutput[1,]),rev(c(quantilesThisOutput[2,1],quantilesThisOutput[2,]))),border=NA,col=paste(col[type],col.alpha,sep=""))
        axis(1,at=labelsAt,labels=labels,cex.lab=1.5, cex.axis=1.5)
      }
    }
    
  }
  
}
