# read stratigraphic ranges
taxa = readTaxonData(file = "ranges.txt")

# read fossil counts
k <- readDataDelimitedFile(file = "presence.absence.txt", header = true, rownames = true)

# interval boundaries
timeline <- v(66.0, 100.5, 145.0, 163.5, 174.1, 201.3, 237)

mvi = 1

alpha <- 10

# smoothing parameters

# estimated sigma
sigma ~ dnExp(alpha)

moves[mvi++] = mvScale(sigma, tune = true)

# specify FBDR model parameters -> youngest interval = 1
for(i in 1:(timeline.size()+1)){

  # sample value (in log space) for the first interval
  if(i == 1){
    
    m[i] ~ dnNorm(0, sigma)
    l[i] ~ dnNorm(0, sigma)

    dt = abs(timeline[i])

    m[i+1] ~ dnNorm(m[i], sigma * dt)
    l[i+1] ~ dnNorm(m[i], sigma * dt)

    mu[i] := (exp( m[i] ) + exp( m[i+1] ) )/ dt
    lambda[i] := (exp( l[i] ) + exp( l[i+1] ) )/ dt

  } else { # last interval

    if(i == timeline.size() + 1) {

        mu[i] := exp( m[i] )
        lambda[i] := exp( l[i] )

    } else {

        dt = abs(timeline[i]-timeline[i-1])

        m[i+1] ~ dnNorm(m[i], sigma * dt)
        l[i+1] ~ dnNorm(m[i], sigma * dt)

        mu[i] := (exp( m[i] ) + exp( m[i+1] ) )/ dt
        lambda[i] := (exp( l[i] ) + exp( l[i+1] ) )/ dt
    }

  }

  moves[mvi++] = mvScale(m[i], tune = true)          
  moves[mvi++] = mvScale(l[i], tune = true)
  
  div[i] := lambda[i] - mu[i]
  turnover[i] := mu[i]/lambda[i]
  
  psi[i] ~ dnExp(alpha)

  moves[mvi++] = mvScale(psi[i], tune = true)

}

moves[mvi++] = mvMultipleElementVectorScale(psi, tune = true, numToMove = 2)

moves[mvi++] = mvVectorScale(psi, tune = true)

rho <- 0

# model 1
bd ~ dnFBDRMatrix(taxa=taxa, lambda=lambda, mu=mu, psi=psi, rho=rho, timeline=timeline, k=k, binary=true)

moves[mvi++] = mvMatrixElementScale(bd, tune = true, weight=(taxa.size()*2))

moves[mvi++] = mvMatrixElementSlide(bd, tune = true, weight=(taxa.size()*2))
      
mymodel = model(bd)

# add monitors
mni = 1

monitors[mni++] = mnScreen(lambda, mu, psi, printgen=4000)
monitors[mni++] = mnModel(filename = "output/model3_scaled.log", printgen=40)

# monitors to print RevGagets input
monitors[mni++] = mnFile(filename = "output/model3_scaled_speciation_rates.log", lambda, printgen=40)
monitors[mni++] = mnFile(filename = "output/model3_scaled_speciation_times.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/model3_scaled_extinction_rates.log", mu, printgen=40)
monitors[mni++] = mnFile(filename = "output/model3_scaled_extinction_times.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/model3_scaled_sampling_rates.log", psi, printgen=40)
monitors[mni++] = mnFile(filename = "output/model3_scaled_sampling_times.log", timeline, printgen=40)

# run the analysis
mymcmc = mcmc(mymodel, moves, monitors, moveschedule = "random")
mymcmc.run(400000)

q()
